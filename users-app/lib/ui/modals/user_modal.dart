import 'package:admin_dashboard/models/http/users_response.dart';
import 'package:admin_dashboard/providers/users_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:admin_dashboard/services/notifications_service.dart';

import 'package:admin_dashboard/ui/buttons/custom_outlined_button.dart';
import 'package:admin_dashboard/ui/inputs/custom_inputs.dart';
import 'package:admin_dashboard/ui/labels/custom_labels.dart';

class UserModal extends StatefulWidget {
  final User? user;

  const UserModal({Key? key, this.user}) : super(key: key);

  @override
  _UserModalState createState() => _UserModalState();
}

class _UserModalState extends State<UserModal> {
  String name = '';
  String age = '';
  String work = '';
  String hobie = '';
  String country = '';
  String? id;

  @override
  void initState() {
    super.initState();

    id = widget.user?.id;
    name = widget.user?.name ?? '';
    age = widget.user?.age.toString() ?? '';
    work = widget.user?.work ?? '';
    hobie = widget.user?.hobie ?? '';
    country = widget.user?.country ?? '';
  }

  @override
  Widget build(BuildContext context) {
    final userProvider = Provider.of<UsersProvider>(context, listen: false);

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30),
      padding: EdgeInsets.all(20),
      height: 500,
      width: 300, // expanded
      decoration: buildBoxDecoration(),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(widget.user?.name ?? 'Nuevo usuario',
                  style: CustomLabels.h1.copyWith(color: Colors.white)),
              IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.white,
                  ),
                  onPressed: () => Navigator.of(context).pop())
            ],
          ),
          Divider(color: Colors.white.withOpacity(0.3)),
          SizedBox(height: 15),
          Row(
            children: [
              Expanded(
                flex: 7,
                child: Container(
                  padding: EdgeInsets.only(right: 15),
                  child: TextFormField(
                    initialValue: widget.user?.name ?? '',
                    onChanged: (value) => name = value,
                    decoration: CustomInputs.loginInputDecoration(
                        hint: 'Nombre usuario',
                        label: 'Nombre',
                        icon: Icons.new_releases_outlined),
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              SizedBox(height: 30),
              Expanded(
                flex: 3,
                child: Container(
                  padding: EdgeInsets.only(left: 15),
                  child: TextFormField(
                    initialValue: widget.user?.age.toString() ?? '',
                    onChanged: (value) => age = value,
                    decoration: CustomInputs.loginInputDecoration(
                        hint: 'Edad usuario',
                        label: 'Edad',
                        icon: Icons.new_releases_outlined),
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 15),
          TextFormField(
            initialValue: widget.user?.work ?? '',
            onChanged: (value) => work = value,
            decoration: CustomInputs.loginInputDecoration(
                hint: 'trabajo del  usuario',
                label: 'Trabajo',
                icon: Icons.new_releases_outlined),
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(height: 15),
          TextFormField(
            initialValue: widget.user?.country ?? '',
            onChanged: (value) => country = value,
            decoration: CustomInputs.loginInputDecoration(
                hint: 'Pais del  usuario',
                label: 'Pais de origen',
                icon: Icons.new_releases_outlined),
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(height: 15),
          TextFormField(
            initialValue: widget.user?.hobie ?? '',
            onChanged: (value) => hobie = value,
            decoration: CustomInputs.loginInputDecoration(
                hint: 'Pasatiempo del  usuario',
                label: 'pasatiempo',
                icon: Icons.new_releases_outlined),
            style: TextStyle(color: Colors.white),
          ),
          Container(
            margin: EdgeInsets.only(top: 30),
            alignment: Alignment.center,
            child: CustomOutlinedButton(
              onPressed: () async {
                try {
                  if (id == null) {
                    // Crear
                    await userProvider.newUser(
                        name, int.parse(age), country, work, hobie);
                    NotificationsService.showSnackbar('$name creado!');
                  } else {
                    // Actualizar
                    await userProvider.updateUser(
                        id!, name, int.parse(age), country, work, hobie);
                    NotificationsService.showSnackbar('$name Actualizado!');
                  }

                  Navigator.of(context).pop();
                } catch (e) {
                  Navigator.of(context).pop();
                  NotificationsService.showSnackbarError(
                      'No se pudo guardar la categoría');
                }
              },
              text: 'Guardar',
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }

  BoxDecoration buildBoxDecoration() => BoxDecoration(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20), topRight: Radius.circular(20)),
      color: Color(0xff0F2041),
      boxShadow: [BoxShadow(color: Colors.black26)]);
}
