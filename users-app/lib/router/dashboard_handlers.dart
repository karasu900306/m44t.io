import 'package:admin_dashboard/ui/views/users_view.dart';
import 'package:fluro/fluro.dart';

import 'package:admin_dashboard/ui/views/blank_view.dart';

class DashboardHandlers {
  static Handler blank = Handler(handlerFunc: (context, params) {
    return BlankView();
  });

  static Handler categories = Handler(handlerFunc: (context, params) {
    return UsersView();
  });
}
