// To parse this JSON data, do
//
//     final usersResponse = usersResponseFromMap(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

UsersResponse usersResponseFromMap(String str) =>
    UsersResponse.fromMap(json.decode(str));

String usersResponseToMap(UsersResponse data) => json.encode(data.toMap());

class UsersResponse {
  UsersResponse({
    required this.users,
  });

  final List<User> users;

  factory UsersResponse.fromMap(Map<String, dynamic> json) => UsersResponse(
        users: List<User>.from(json["users"].map((x) => User.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "users": List<dynamic>.from(users.map((x) => x.toMap())),
      };
}

class User {
  User({
    required this.id,
    required this.name,
    required this.age,
    required this.hobie,
    required this.work,
    required this.country,
  });

  String id;
  String name;
  int age;
  String hobie;
  String work;
  String country;

  factory User.fromMap(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        age: json["age"],
        hobie: json["hobie"],
        work: json["work"],
        country: json["country"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "age": age,
        "hobie": hobie,
        "work": work,
        "country": country,
      };
}
