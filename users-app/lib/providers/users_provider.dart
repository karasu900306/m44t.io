import 'package:admin_dashboard/models/http/users_response.dart';
import 'package:flutter/material.dart';

import 'package:admin_dashboard/api/CafeApi.dart';

import 'package:admin_dashboard/models/usuario.dart';

class UsersProvider extends ChangeNotifier {
  List<User> users = [];
  bool isLoading = true;
  bool ascending = true;
  int? sortColumnIndex;

  UsersProvider() {
    this.getPaginatedUsers();
  }

  getPaginatedUsers() async {
    final resp = await CafeApi.httpGet('/users');
    final usersResp = UsersResponse.fromMap(resp);
    this.users = [...usersResp.users];
    isLoading = false;
    notifyListeners();
  }

  Future<Usuario> getUserById(String uid) async {
    try {
      final resp = await CafeApi.httpGet('/usuarios/$uid');
      final user = Usuario.fromMap(resp);
      return user;
    } catch (e) {
      print(e);
      throw e;
    }
  }

  Future newUser(
      String name, int age, String country, String work, String hobie) async {
    final data = {
      'name': name,
      'age': age,
      'country': country,
      'work': work,
      "hobie": hobie
    };

    try {
      final json = await CafeApi.post('/users', data);
      final newUser = User.fromMap(json);

      users.add(newUser);
      notifyListeners();
    } catch (e) {
      print(e);
      throw 'Error al crear usuario';
    }
  }

  Future updateUser(String id, String name, int age, String country,
      String work, String hobie) async {
    final data = {
      'name': name,
      'age': age,
      'country': country,
      'work': work,
      "hobie": hobie
    };

    try {
      await CafeApi.put('/users/$id', data);

      this.users = this.users.map((user) {
        if (user.id != id) return user;
        user.name = name;
        user.age = age;
        user.country = country;
        user.work = work;
        user.hobie = hobie;
        return user;
      }).toList();

      notifyListeners();
    } catch (e) {
      throw 'Error al crear categoria';
    }
  }

  Future deleteUser(String id) async {
    try {
      await CafeApi.delete('/users/$id', {});

      users.removeWhere((user) => user.id == id);

      notifyListeners();
    } catch (e) {
      print(e);
      print('Error al borrar usuario');
    }
  }

  void sort<T>(Comparable<T> Function(User user) getField) {
    users.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);

      return ascending
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });

    ascending = !ascending;

    notifyListeners();
  }

  void refreshUser(User newUser) {
    this.users = this.users.map((user) {
      if (user.id == newUser.id) {
        user = newUser;
      }

      return user;
    }).toList();

    notifyListeners();
  }
}
