import 'package:admin_dashboard/api/CafeApi.dart';
import 'package:admin_dashboard/models/http/users_response.dart';
import 'package:admin_dashboard/models/usuario.dart';
import 'package:flutter/material.dart';

class UserFormProvider extends ChangeNotifier {
  User? user;
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  // void updateListener() {
  //   notifyListeners();
  // }
  copyUserWith({
    String? id,
    String? name,
    int? age,
    String? hobie,
    String? work,
    String? country,
  }) {
    user = new User(
      id: id ?? this.user!.id,
      name: name ?? this.user!.name,
      age: age ?? this.user!.age,
      hobie: hobie ?? this.user!.hobie,
      work: work ?? this.user!.work,
      country: country ?? this.user!.country,
    );
    notifyListeners();
  }

  bool _validForm() {
    return formKey.currentState!.validate();
  }

  Future updateUser() async {
    if (!this._validForm()) return false;

    final data = {
      'name': user!.name,
      'age': user!.age,
    };

    try {
      final resp = await CafeApi.put('/users/${user!.id}', data);
      print(resp);
      return true;
    } catch (e) {
      print('error en updateUser: $e');
      return false;
    }
  }
}
