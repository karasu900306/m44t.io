import 'package:admin_dashboard/models/http/users_response.dart';
import 'package:admin_dashboard/providers/users_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:admin_dashboard/ui/modals/user_modal.dart';

class CategoriesDTS extends DataTableSource {
  final List<User> users;
  final BuildContext context;

  CategoriesDTS(this.users, this.context);

  @override
  DataRow getRow(int index) {
    final user = this.users[index];

    return DataRow.byIndex(index: index, cells: [
      DataCell(Text(user.name)),
      DataCell(Text(user.age.toString())),
      DataCell(Text(user.hobie)),
      DataCell(Text(user.work)),
      DataCell(Text(user.country)),
      DataCell(Row(
        children: [
          IconButton(
              icon: Icon(Icons.edit_outlined),
              onPressed: () {
                showModalBottomSheet(
                    backgroundColor: Colors.transparent,
                    context: context,
                    builder: (_) => UserModal(user: user));
              }),
          IconButton(
              icon: Icon(Icons.delete_outline,
                  color: Colors.red.withOpacity(0.8)),
              onPressed: () {
                final dialog = AlertDialog(
                  title: Text('¿Está seguro de borrarlo?'),
                  content: Text('¿Borrar definitivamente ${user.name}?'),
                  actions: [
                    TextButton(
                      child: Text('No'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                    TextButton(
                      child: Text('Si, borrar'),
                      onPressed: () async {
                        await Provider.of<UsersProvider>(context, listen: false)
                            .deleteUser(user.id);

                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );

                showDialog(context: context, builder: (_) => dialog);
              }),
        ],
      )),
    ]);
  }

  @override
  bool get isRowCountApproximate => false;

  @override
  int get rowCount => users.length;

  @override
  int get selectedRowCount => 0;
}
