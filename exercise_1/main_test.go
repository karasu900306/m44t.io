package main

import (
	"reflect"
	"testing"
)

func Test_getNumbersAndCharacteres(t *testing.T) {
	type args struct {
		line string
	}
	tests := []struct {
		name string
		args args
		want response
	}{
		{
			name: "success",
			args: args{
				line: `"?",56,30,"-",21,".",55,"+",":",54,"^",23,45,"$","&",2`,
			},
			want: response{
				Numbers:    []int{56, 55, 54, 45, 30, 23, 21, 2},
				Characters: []string{"?", "-", ".", "+", ":", "^", "$", "&"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getNumbersAndCharacteres(tt.args.line); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getNumbersAndCharacteres() = %v, want %v", got, tt.want)
			}
		})
	}
}
