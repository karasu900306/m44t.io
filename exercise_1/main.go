package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"sort"
	"strconv"
	"strings"
)

type response struct {
	Numbers    []int
	Characters []string
}

func main() {

	filename := "ejercice1.txt"
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	res := getNumbersAndCharacteres(string(file))

	resBin, err := json.Marshal(res)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(resBin))
}

func getNumbersAndCharacteres(line string) response {
	line = strings.TrimSpace(line)
	elements := strings.Split(line, ",")
	numbers := make([]int, 0)
	characters := make([]string, 0)
	for _, element := range elements {
		number, err := strconv.Atoi(element)
		if err != nil {
			character := strings.ReplaceAll(element, `"`, "")
			fmt.Println(character)
			characters = append(characters, character)
			continue
		}

		numbers = append(numbers, number)
	}

	sort.Slice(numbers, func(i, j int) bool { return numbers[i] > numbers[j] })

	return response{
		Numbers:    numbers,
		Characters: characters,
	}
}
