package middleware

import "github.com/gin-gonic/gin"

// Mid contains all middlewares used.
func Mid(r *gin.Engine) {
	r.Use(Cors())
}
