package config

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/maat.io/api/pkg/users/api/handlers"
	"gitlab.com/maat.io/api/pkg/users/core/business"
	"gitlab.com/maat.io/api/pkg/users/persistence/nosql"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/yaml.v2"
)

type Config struct {
	MongoDB struct {
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
		Database string `yaml:"database"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
	} `yaml:"db"`
}

// ConnectURL returns the one liner connection string for postgres.
func (c *Config) ConnectMongoURL() string {
	return fmt.Sprintf("mongodb+srv://%s:%s@%s", c.MongoDB.User, c.MongoDB.Password, c.MongoDB.Host)
}

func LoadConfig(file string) (*Config, error) {
	if file == "" {
		return nil, errors.New("empty filename")
	}
	// #nosec
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	if err := f.Close(); err != nil {
		return nil, err
	}
	var c *Config
	if err := yaml.Unmarshal(b, &c); err != nil {
		return nil, err
	}
	return c, nil
}

func SetLayers(db *mongo.Database, config *Config) {

	usersRepo := nosql.NewUsersRepo(db, "users")

	userBusiness := business.NewUserBusinessLogic(usersRepo)

	handlers.NewUsersApiHandler(userBusiness)
}
