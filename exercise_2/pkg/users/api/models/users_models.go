package models

type User struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Age     uint8  `json:"age"`
	Hobie   string `json:"hobie"`
	Work    string `json:"work"`
	Country string `json:"country"`
}

type FiltersUser struct {
	UserID  string
	UserIDs []string
}

type UserCreateResponse struct {
	UserID string `json:"id" uri:"id" binding:"required"`
}

type UserGetResponse struct {
	Users []*User `json:"users"`
}
