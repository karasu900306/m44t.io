package handlers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/maat.io/api/pkg/users/core/mocks"
	modelsCore "gitlab.com/maat.io/api/pkg/users/core/models"
	"gitlab.com/maat.io/api/pkg/users/core/services"
)

func TestUsersApiHandler_CreateUser(t *testing.T) {
	userServiceMock := new(mocks.IUserServices)
	user := &modelsCore.User{
		ID:      "id",
		Name:    "Masiel",
		Age:     31,
		Hobie:   "anime",
		Work:    "programmer",
		Country: "Mexico",
	}
	userServiceMock.On("SetUser", mock.Anything, user).Return("id", nil).Once()

	userServiceMockError := new(mocks.IUserServices)
	userServiceMockError.On("SetUser", mock.Anything, user).Return("", assert.AnError).Once()

	bin, err := json.Marshal(user)
	assert.NoError(t, err)

	table := []struct {
		Purpose      string
		ExpCode      int
		UsersService services.IUserServices
		*http.Request
		ExpBody string
	}{
		{
			"1. OK: CreateUser",
			http.StatusOK,
			userServiceMock,
			newRequest("POST", usersBasePath, string(bin)),
			`{"id":"id"}`,
		},
		{
			"2. error: CreateUser",
			http.StatusInternalServerError,
			userServiceMockError,
			newRequest("POST", usersBasePath, string(bin)),
			`{"message":"internal error"}`,
		},
	}
	for _, x := range table {
		r := gin.New()
		NewUsersApiHandler(x.UsersService)
		ConfigUsersApiRoutes(r)
		w := httptest.NewRecorder()
		r.ServeHTTP(w, x.Request)
		assert.EqualValues(t, x.ExpCode, w.Code, x.Purpose)

		actual := w.Body.String()
		assert.EqualValues(t, x.ExpBody, actual, x.Purpose, actual)
	}
}

func newRequest(method, uri, body string) *http.Request {
	req, _ := http.NewRequest(method, uri, bytes.NewBufferString(body))
	ctx := req.Context()
	return req.WithContext(ctx)
}
