package handlers

import (
	"errors"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/maat.io/api/pkg/users/api/mappers"
	"gitlab.com/maat.io/api/pkg/users/api/models"
	"gitlab.com/maat.io/api/pkg/users/core/services"
)

const (
	version        = "/v1"
	usersBasePath  = version + "/users"
	updateUserPath = "/:id"
)

type UsersApiHandler struct {
	usersServices services.IUserServices
}

var handler *UsersApiHandler

func NewUsersApiHandler(usersServices services.IUserServices) {
	handler = &UsersApiHandler{usersServices: usersServices}
}

func ConfigUsersApiRoutes(ctx *gin.Engine) {
	usersGroup := ctx.Group(usersBasePath)
	usersGroup.POST("", handler.CreateUser)
	usersGroup.GET("", handler.GetUsers)
	usersGroup.PUT(updateUserPath, handler.UpdateUser)
	usersGroup.DELETE(updateUserPath, handler.DeleteUsers)
}

func (u UsersApiHandler) CreateUser(ctx *gin.Context) {
	var body models.User
	if err := ctx.BindJSON(&body); err != nil {
		log.Printf("ERROR: handler:users:CreateUser:Bind error:%s", err.Error())
		ctx.AbortWithStatusJSON(http.StatusBadRequest, errors.New("error parsing parameter"))

		return
	}

	userID, err := u.usersServices.SetUser(ctx.Request.Context(), mappers.MapperUserAPIToService(&body))
	if err != nil {
		log.Printf("ERROR: handler:users:CreateUser:SetUser error:%s", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "internal error",
		})

		return
	}

	body.ID = userID

	ctx.JSON(http.StatusOK, body)

}

func (u UsersApiHandler) GetUsers(ctx *gin.Context) {
	var query models.FiltersUser
	err := ctx.BindQuery(&query)
	if err != nil {
		log.Printf("ERROR: handler:users:GetUsers:BindQuery error:%s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "bad request",
		})

		return
	}

	users, err := u.usersServices.GetUsers(ctx.Request.Context(), mappers.MapperUserFilterAPIToService(&query))
	if err != nil {
		log.Printf("ERROR: handler:users:GetUsers:GetUsers error:%s", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "internal error",
		})

		return
	}

	ctx.JSON(http.StatusOK, models.UserGetResponse{Users: mappers.MapperUsersServiceToApi(users)})
}

func (u UsersApiHandler) UpdateUser(ctx *gin.Context) {
	var query models.UserCreateResponse
	err := ctx.ShouldBindUri(&query)
	if err != nil {
		log.Printf("ERROR: handler:users:DeleteUsers:ShouldBindUri error:%s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "bad request",
		})

		return
	}
	var body models.User
	if err := ctx.BindJSON(&body); err != nil {
		log.Printf("ERROR: handler:users:UpdateUser:BindJSON error:%s", err.Error())
		ctx.AbortWithStatusJSON(http.StatusBadRequest, errors.New("error parsing parameter"))

		return
	}

	body.ID = query.UserID
	err = u.usersServices.UpdateUser(ctx.Request.Context(), mappers.MapperUserAPIToService(&body))
	if err != nil {
		log.Printf("ERROR: handler:users:UpdateUser:UpdateUser error:%s", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "internal error",
		})

		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"result": "success",
	})
}

func (u UsersApiHandler) DeleteUsers(ctx *gin.Context) {
	var query models.UserCreateResponse
	err := ctx.ShouldBindUri(&query)
	if err != nil {
		log.Printf("ERROR: handler:users:DeleteUsers:ShouldBindUri error:%s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "bad request",
		})

		return
	}

	err = u.usersServices.DeleteUser(ctx.Request.Context(), query.UserID)
	if err != nil {
		log.Printf("ERROR: handler:users:DeleteUsers:SetUser error:%s", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "internal error",
		})

		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"result": "success",
	})
}
