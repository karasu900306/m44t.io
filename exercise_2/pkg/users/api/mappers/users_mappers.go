package mappers

import (
	"gitlab.com/maat.io/api/pkg/users/api/models"
	coreUserModels "gitlab.com/maat.io/api/pkg/users/core/models"
)

func MapperUsersServiceToApi(users []*coreUserModels.User) []*models.User {
	mapper := make([]*models.User, 0)
	for _, user := range users {
		mapper = append(mapper, (*models.User)(user))
	}

	return mapper
}

func MapperUserAPIToService(user *models.User) *coreUserModels.User {
	return (*coreUserModels.User)(user)
}

func MapperUserFilterAPIToService(user *models.FiltersUser) *coreUserModels.FiltersUser {
	return (*coreUserModels.FiltersUser)(user)
}
