package business

import (
	"context"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/maat.io/api/pkg/users/core/models"
	"gitlab.com/maat.io/api/pkg/users/persistence/mocks"
	userModelsRepo "gitlab.com/maat.io/api/pkg/users/persistence/models"
	"gitlab.com/maat.io/api/pkg/users/persistence/repo"
)

func TestUserBusinessLogic_SetUser(t *testing.T) {
	mockUserRepo := new(mocks.IUserRepo)
	user := &userModelsRepo.User{
		ID:      "id",
		Name:    "Masiel",
		Age:     31,
		Hobie:   "anime",
		Work:    "programmer",
		Country: "Mexico",
	}
	mockUserRepo.On("SetUser", mock.Anything, user).Return("id", nil).Once()

	mockUserRepoError := new(mocks.IUserRepo)
	mockUserRepoError.On("SetUser", mock.Anything, user).Return("", assert.AnError).Once()

	type fields struct {
		userRepo repo.IUserRepo
	}
	type args struct {
		ctx  context.Context
		user *models.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "sucess",
			fields: fields{
				userRepo: mockUserRepo,
			},
			args: args{
				ctx: context.Background(),
				user: &models.User{
					ID:      "id",
					Name:    "Masiel",
					Age:     31,
					Hobie:   "anime",
					Work:    "programmer",
					Country: "Mexico",
				},
			},
			want:    "id",
			wantErr: false,
		},
		{
			name: "error repo",
			fields: fields{
				userRepo: mockUserRepoError,
			},
			args: args{
				ctx: context.Background(),
				user: &models.User{
					ID:      "id",
					Name:    "Masiel",
					Age:     31,
					Hobie:   "anime",
					Work:    "programmer",
					Country: "Mexico",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ubl := NewUserBusinessLogic(tt.fields.userRepo)
			got, err := ubl.SetUser(tt.args.ctx, tt.args.user)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserBusinessLogic.SetUser() = %v, want %v", got, tt.want)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("UserBusinessLogic.SetUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserBusinessLogic_GetUsers(t *testing.T) {
	mockUserRepo := new(mocks.IUserRepo)
	users := []*userModelsRepo.User{
		{
			ID:      "id",
			Name:    "Masiel",
			Age:     31,
			Hobie:   "anime",
			Work:    "programmer",
			Country: "Mexico",
		},
	}
	filter := &userModelsRepo.FiltersUser{
		UserID: "id",
	}
	mockUserRepo.On("GetUsers", mock.Anything, filter).Return(users, nil).Once()

	mockUserRepoError := new(mocks.IUserRepo)
	mockUserRepoError.On("GetUsers", mock.Anything, filter).Return(nil, assert.AnError).Once()

	type fields struct {
		userRepo repo.IUserRepo
	}
	type args struct {
		ctx     context.Context
		filters *models.FiltersUser
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []*models.User
		wantErr bool
	}{
		{
			name: "sucess",
			fields: fields{
				userRepo: mockUserRepo,
			},
			args: args{
				ctx: context.Background(),
				filters: &models.FiltersUser{
					UserID: "id",
				},
			},
			want: []*models.User{
				{
					ID:      "id",
					Name:    "Masiel",
					Age:     31,
					Hobie:   "anime",
					Work:    "programmer",
					Country: "Mexico",
				},
			},
			wantErr: false,
		},
		{
			name: "error repo",
			fields: fields{
				userRepo: mockUserRepoError,
			},
			args: args{
				ctx: context.Background(),
				filters: &models.FiltersUser{
					UserID: "id",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ubl := UserBusinessLogic{
				userRepo: tt.fields.userRepo,
			}
			got, err := ubl.GetUsers(tt.args.ctx, tt.args.filters)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserBusinessLogic.GetUsers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserBusinessLogic.GetUsers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserBusinessLogic_UpdateUser(t *testing.T) {
	mockUserRepo := new(mocks.IUserRepo)
	user := &userModelsRepo.User{
		ID:      "id",
		Name:    "Masiel",
		Age:     31,
		Hobie:   "anime",
		Work:    "programmer",
		Country: "Mexico",
	}
	mockUserRepo.On("UpdateUser", mock.Anything, user).Return(nil).Once()

	mockUserRepoError := new(mocks.IUserRepo)
	mockUserRepoError.On("UpdateUser", mock.Anything, user).Return(assert.AnError).Once()

	type fields struct {
		userRepo repo.IUserRepo
	}
	type args struct {
		ctx  context.Context
		user *models.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				userRepo: mockUserRepo,
			},
			args: args{
				ctx: context.Background(),
				user: &models.User{
					ID:      "id",
					Name:    "Masiel",
					Age:     31,
					Hobie:   "anime",
					Work:    "programmer",
					Country: "Mexico",
				},
			},
			wantErr: false,
		},
		{
			name: "error repo",
			fields: fields{
				userRepo: mockUserRepoError,
			},
			args: args{
				ctx: context.Background(),
				user: &models.User{
					ID:      "id",
					Name:    "Masiel",
					Age:     31,
					Hobie:   "anime",
					Work:    "programmer",
					Country: "Mexico",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ubl := UserBusinessLogic{
				userRepo: tt.fields.userRepo,
			}
			if err := ubl.UpdateUser(tt.args.ctx, tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("UserBusinessLogic.UpdateUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserBusinessLogic_DeleteUser(t *testing.T) {
	mockUserRepo := new(mocks.IUserRepo)
	mockUserRepo.On("DeleteUser", mock.Anything, "id").Return(nil).Once()

	mockUserRepoError := new(mocks.IUserRepo)
	mockUserRepoError.On("DeleteUser", mock.Anything, "id").Return(assert.AnError).Once()

	type fields struct {
		userRepo repo.IUserRepo
	}
	type args struct {
		ctx    context.Context
		userID string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				userRepo: mockUserRepo,
			},
			args: args{
				ctx:    context.Background(),
				userID: "id",
			},
			wantErr: false,
		},
		{
			name: "error repo",
			fields: fields{
				userRepo: mockUserRepoError,
			},
			args: args{
				ctx:    context.Background(),
				userID: "id",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ubl := UserBusinessLogic{
				userRepo: tt.fields.userRepo,
			}
			if err := ubl.DeleteUser(tt.args.ctx, tt.args.userID); (err != nil) != tt.wantErr {
				t.Errorf("UserBusinessLogic.DeleteUser() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
