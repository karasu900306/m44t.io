package business

import (
	"context"
	"fmt"

	"gitlab.com/maat.io/api/pkg/users/core/mappers"
	"gitlab.com/maat.io/api/pkg/users/core/models"
	"gitlab.com/maat.io/api/pkg/users/persistence/repo"
)

type UserBusinessLogic struct {
	userRepo repo.IUserRepo
}

func NewUserBusinessLogic(userRepo repo.IUserRepo) *UserBusinessLogic {
	return &UserBusinessLogic{userRepo: userRepo}
}

func (ubl UserBusinessLogic) SetUser(ctx context.Context, user *models.User) (string, error) {
	userID, err := ubl.userRepo.SetUser(ctx, mappers.MapperUserServiceToRepo(user))
	if err != nil {
		fmt.Printf("SetUser services error: %s", err.Error())
		return "", err
	}

	return userID, nil
}

func (ubl UserBusinessLogic) GetUsers(ctx context.Context, filters *models.FiltersUser) ([]*models.User, error) {
	users, err := ubl.userRepo.GetUsers(ctx, mappers.MapperUserFilterServiceToRepo(filters))
	if err != nil {
		fmt.Printf("GetUsers services error: %s", err.Error())
		return nil, err
	}

	return mappers.MapperUsersRepoToService(users), nil
}

func (ubl UserBusinessLogic) UpdateUser(ctx context.Context, user *models.User) error {
	err := ubl.userRepo.UpdateUser(ctx, mappers.MapperUserServiceToRepo(user))
	if err != nil {
		fmt.Printf("UpdateUser services error: %s", err.Error())
		return err
	}

	return nil
}

func (ubl UserBusinessLogic) DeleteUser(ctx context.Context, userID string) error {
	err := ubl.userRepo.DeleteUser(ctx, userID)
	if err != nil {
		fmt.Printf("DeleteUser services error: %s", err.Error())
		return err
	}
	return nil
}
