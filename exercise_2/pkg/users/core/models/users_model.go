package models

type User struct {
	ID      string
	Name    string
	Age     uint8
	Hobie   string
	Work    string
	Country string
}

type FiltersUser struct {
	UserID  string
	UserIDs []string
}
