package services

import (
	"context"

	"gitlab.com/maat.io/api/pkg/users/core/models"
)

type IUserServices interface {
	SetUser(ctx context.Context, user *models.User) (string, error)
	GetUsers(ctx context.Context, filters *models.FiltersUser) ([]*models.User, error)
	UpdateUser(ctx context.Context, user *models.User) error
	DeleteUser(ctx context.Context, userID string) error
}
