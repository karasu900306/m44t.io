package mappers

import (
	"reflect"
	"testing"

	"gitlab.com/maat.io/api/pkg/users/core/models"
	repoModels "gitlab.com/maat.io/api/pkg/users/persistence/models"
)

func TestMapperUsersRepoToService(t *testing.T) {
	type args struct {
		users []*repoModels.User
	}
	tests := []struct {
		name string
		args args
		want []*models.User
	}{
		{
			name: "success",
			args: args{
				users: []*repoModels.User{
					{
						ID:      "id",
						Name:    "Masiel",
						Age:     31,
						Hobie:   "anime",
						Work:    "programmer",
						Country: "Mexico",
					},
				},
			},
			want: []*models.User{
				{
					ID:      "id",
					Name:    "Masiel",
					Age:     31,
					Hobie:   "anime",
					Work:    "programmer",
					Country: "Mexico",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MapperUsersRepoToService(tt.args.users); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MapperUsersRepoToService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMapperUserServiceToRepo(t *testing.T) {
	type args struct {
		user *models.User
	}
	tests := []struct {
		name string
		args args
		want *repoModels.User
	}{
		{
			name: "success",
			args: args{
				user: &models.User{
					ID:      "id",
					Name:    "Masiel",
					Age:     31,
					Hobie:   "anime",
					Work:    "programmer",
					Country: "Mexico",
				},
			},
			want: &repoModels.User{
				ID:      "id",
				Name:    "Masiel",
				Age:     31,
				Hobie:   "anime",
				Work:    "programmer",
				Country: "Mexico",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MapperUserServiceToRepo(tt.args.user); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MapperUserServiceToRepo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMapperUserFilterServiceToRepo(t *testing.T) {
	type args struct {
		user *models.FiltersUser
	}
	tests := []struct {
		name string
		args args
		want *repoModels.FiltersUser
	}{
		{
			name: "success",
			args: args{
				user: &models.FiltersUser{
					UserID: "id",
				},
			},
			want: &repoModels.FiltersUser{
				UserID: "id",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MapperUserFilterServiceToRepo(tt.args.user); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MapperUserFilterServiceToRepo() = %v, want %v", got, tt.want)
			}
		})
	}
}
