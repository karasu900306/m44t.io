package mappers

import (
	"gitlab.com/maat.io/api/pkg/users/core/models"
	repoModels "gitlab.com/maat.io/api/pkg/users/persistence/models"
)

func MapperUsersRepoToService(users []*repoModels.User) []*models.User {
	mapper := make([]*models.User, 0)
	for _, user := range users {
		mapper = append(mapper, (*models.User)(user))
	}

	return mapper
}

func MapperUserServiceToRepo(user *models.User) *repoModels.User {
	return (*repoModels.User)(user)
}

func MapperUserFilterServiceToRepo(user *models.FiltersUser) *repoModels.FiltersUser {
	return (*repoModels.FiltersUser)(user)
}
