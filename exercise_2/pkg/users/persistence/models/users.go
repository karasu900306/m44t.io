package models

type User struct {
	ID      string `json:"id" bson:"_id,omitempty"`
	Name    string
	Age     uint8
	Hobie   string
	Work    string
	Country string
}

type FiltersUser struct {
	UserID  string
	UserIDs []string
}
