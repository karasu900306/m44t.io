package nosql

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/maat.io/api/pkg/users/persistence/models"
)

type UsersRepo struct {
	db *mongo.Collection
}

func NewUsersRepo(db *mongo.Database, collection string) *UsersRepo {
	return &UsersRepo{db: db.Collection(collection)}
}

func (ur UsersRepo) SetUser(ctx context.Context, user *models.User) (string, error) {
	result, err := ur.db.InsertOne(ctx, user)
	if err != nil {
		log.Printf("error insert user %s", err.Error())
		return "", err
	}

	return result.InsertedID.(primitive.ObjectID).Hex(), nil
}

func (ur UsersRepo) GetUsers(ctx context.Context, filters *models.FiltersUser) ([]*models.User, error) {
	filter := bson.D{}
	userCollection, err := ur.db.Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	users := make([]*models.User, 0)
	for userCollection.Next(ctx) {
		user := new(models.User)
		err = userCollection.Decode(user)
		if err != nil {
			return nil, err
		}

		users = append(users, user)
	}

	return users, nil
}

func (ur UsersRepo) UpdateUser(ctx context.Context, user *models.User) error {
	oid, err := primitive.ObjectIDFromHex(user.ID)
	if err != nil {
		log.Printf("error object id %s", err.Error())
		return err
	}

	filter := bson.M{"_id": oid}
	update := bson.M{
		"$set": bson.M{
			"age":     user.Age,
			"name":    user.Name,
			"hobie":   user.Hobie,
			"work":    user.Work,
			"country": user.Country,
		},
	}
	_, err = ur.db.UpdateOne(ctx, filter, update)
	if err != nil {
		log.Printf("error update user %s", err.Error())
		return err
	}

	return nil
}

func (ur UsersRepo) DeleteUser(ctx context.Context, userID string) error {
	oid, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		log.Printf("error object id %s", err.Error())
		return err
	}

	filter := bson.M{"_id": oid}
	_, err = ur.db.DeleteOne(ctx, filter)
	if err != nil {
		log.Printf("error delete user %s", err.Error())
		return err
	}

	return nil
}
