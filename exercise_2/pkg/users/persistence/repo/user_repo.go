package repo

import (
	"context"

	"gitlab.com/maat.io/api/pkg/users/persistence/models"
)

type IUserRepo interface {
	SetUser(ctx context.Context, user *models.User) (string, error)
	GetUsers(ctx context.Context, filters *models.FiltersUser) ([]*models.User, error)
	UpdateUser(ctx context.Context, user *models.User) error
	DeleteUser(ctx context.Context, userID string) error
}
