package main

import (
	"flag"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/maat.io/api/pkg/config"
	"gitlab.com/maat.io/api/pkg/dai"
	"gitlab.com/maat.io/api/pkg/middleware"
	"gitlab.com/maat.io/api/pkg/users/api/handlers"
)

var (
	port  = flag.Int("port", 8080, "Listen port.")
	cFile = flag.String("config", "", "Configuration file YML.")
)

func main() {

	flag.Parse()
	log.Println(*port)
	conf, err := config.LoadConfig(*cFile)
	if err != nil {
		log.Fatal(err)
		panic("Error on ")
	}

	mongodb := dai.CreateConnection(conf.ConnectMongoURL(), conf.MongoDB.Database)

	config.SetLayers(mongodb, conf)

	r := gin.New()
	middleware.Mid(r)

	handlers.ConfigUsersApiRoutes(r)

	if err := r.Run(":" + strconv.Itoa(*port)); err != nil {
		log.Fatal(err)
	}

}
