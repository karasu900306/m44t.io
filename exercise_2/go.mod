module gitlab.com/maat.io/api

go 1.14

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/klauspost/compress v1.10.7 // indirect
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.7.0
	golang.org/x/sys v0.0.0-20200602225109-6fdc65e7d980 // indirect
	gopkg.in/yaml.v2 v2.2.8
)
