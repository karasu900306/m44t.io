package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"time"
)

type TestStruct struct {
	Kind            string  `json:"kind"`
	Valor           Valor   `json:"valor"`
	Valor2          []int64 `json:"valor_2"`
	ExternalComment string  `json:"externalComment"`
}
type Valor struct {
	IDVal               int64     `json:"idVal"`
	Client              int64     `json:"client"`
	Kind                string    `json:"kind"`
	Caduca              time.Time `json:"Caduca"`
	Creation            time.Time `json:"creation"`
	RequestedCategories []int64   `json:"requestedCategories"`
	RequestedTemplates  []int64   `json:"requested_templates"`
	Status              string    `json:"status"`
}

func main() {
	var testStruct TestStruct
	request := `{"kind": "MAAT","valor": {"idVal": 5634284113100800,"client": 5680219105001472,"kind": "cel","Caduca": "2051-06-18T22:24:56.891316558Z","creation": "2021-06-18T22:24:56.891319236Z","requestedCategories": [5670888275968000],"requested_templates": [5676096687177728,5722001280860160,5742670936801280],"status": "created"},"externalComment":"test_external_id"}`
	err := json.Unmarshal([]byte(request), &testStruct)
	if err != nil {
		panic(err)
	}
	log.Println(testStruct)
	testStruct.Valor2 = getValor2()
	log.Println(testStruct)

	log.Println(FormatSince(testStruct.Valor.Creation))
}

func getValor2() []int64 {

	valor2 := make([]int64, 0)
	min := 0
	max := 3000000
	for i := 0; i < 5; i++ {
		valor2 = append(valor2, int64(rand.Intn(max-min)+min))
	}
	return valor2
}

func FormatSince(t time.Time) string {
	const (
		Decisecond = 100 * time.Millisecond
		Day        = 24 * time.Hour
	)
	ts := time.Since(t)
	sign := time.Duration(1)
	if ts < 0 {
		sign = -1
		ts = -ts
	}
	ts += +Decisecond / 2
	d := sign * (ts / Day)
	ts = ts % Day
	h := ts / time.Hour
	ts = ts % time.Hour
	m := ts / time.Minute
	ts = ts % time.Minute
	s := ts / time.Second
	ts = ts % time.Second
	f := ts / Decisecond
	return fmt.Sprintf("%dday%dhours%dminutes%d.%dseconds", d, h, m, s, f)
}
